//
//  TVCCollection.swift
//  Actividad_login
//
//  Created by Jaime García Castán on 28/3/17.
//  Copyright © 2017 Jaime García Castán. All rights reserved.
//

import UIKit
 
class CVCItem2: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {
    
    @IBOutlet var miColeccion:UICollectionView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "miCelda2", for: indexPath)as!
        CVCCell
    
        cell.lbl?.text = ""
        for _ in 0 ..< 5 {
            cell.lbl?.text = "Jimi\(indexPath.row)"
            if(indexPath.row == 0 || indexPath.row == 3 || indexPath.row == 4){
                cell.img?.image=UIImage(named: "val2.png")
            }
        }
        return cell
    }
}
