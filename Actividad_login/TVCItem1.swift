//
//  TVCcell.swift
//  Actividad_login
//
//  Created by Jaime García Castán on 23/3/17.
//  Copyright © 2017 Jaime García Castán. All rights reserved.
//

import UIKit

class TVCItem1: UIViewController,UITableViewDelegate,UITableViewDataSource{
    @IBOutlet var miTabla:UITableView?
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:TVCCell = tableView.dequeueReusableCell(withIdentifier: "miCelda", for: indexPath)as! TVCCell
        
        cell.lbl?.text = ""
        for _ in 0 ..< 5 {
            cell.lbl?.text = "Jimi \(indexPath.row)"
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        NSLog("HE PINCHADO EN : %d" ,indexPath.row)
    }
}
