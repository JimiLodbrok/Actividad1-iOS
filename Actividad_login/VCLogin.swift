//
//  VCLogin.swift
//  Actividad_login
//
//  Created by Jaime García Castán on 23/3/17.
//  Copyright © 2017 Jaime García Castán. All rights reserved.
//

import UIKit

class VCLogin: UIViewController {
    @IBOutlet var txtUser:UITextField?
    @IBOutlet var txtPass:UITextField?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func logearse(){
        
       // print("1: ",DataHolder.loger.userName, " ",DataHolder.loger.passWord, " COMPARAMOS CON ",(txtUser?.text)!," ",(txtPass?.text)!)
        
        if((txtUser?.text == DataHolder.loger.userName) && (txtPass?.text == DataHolder.loger.passWord) && !((txtUser?.text?.isEmpty)!) && !((txtPass?.text?.isEmpty)!)) {
            self.performSegue(withIdentifier: "transition", sender: self)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
