//
//  VCRegister.swift
//  Actividad_login
//
//  Created by Jaime García Castán on 23/3/17.
//  Copyright © 2017 Jaime García Castán. All rights reserved.
//

import UIKit

class VCRegister: UIViewController {
    
    @IBOutlet var txtUser:UITextField?
    @IBOutlet var txtMail:UITextField?
    @IBOutlet var txtPass:UITextField?
    @IBOutlet var txtRPass:UITextField?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func registrarse(){
        if(!((txtUser?.text?.isEmpty)!) && !((txtPass?.text?.isEmpty)!) && !((txtMail?.text?.isEmpty)!) && !((txtRPass?.text?.isEmpty)!) && (txtPass?.text == txtRPass?.text)) {
            DataHolder.loger.userName = txtUser?.text
            DataHolder.loger.passWord = txtPass?.text
            DataHolder.loger.e_mail = txtMail?.text
            self.performSegue(withIdentifier: "transitionlog", sender: self)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
